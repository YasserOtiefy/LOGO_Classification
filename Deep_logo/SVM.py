# The MIT License (MIT)
# Copyright (c) 2016 satojkovic

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import tensorflow as tf
import numpy as np
from six.moves import cPickle as pickle
from six.moves import range
import sys
import os
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn.decomposition import PCA



max_steps = 10000
image_width = 64
image_height = 32
num_classes = 27
learning_rate = 0.0001
batch_size = 64
num_channels = 3
patch_size = 5
train_dir = "flickr_logos_27_dataset"


PICKLE_FILENAME = 'deep_logo.pickle'





def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_width,image_height,
                               num_channels)).astype(np.float32)
    labels = (
        np.arange(num_classes) == labels[:, None]).astype(np.float32)
    return dataset, labels


def train(data, labels):

    lr = LogisticRegression().fit(data, labels)
    yhat = lr.predict(data)

    print("Train Accuracy is:",accuracy_score(labels, yhat))

    return lr


def read_data():
    with open(PICKLE_FILENAME, 'rb') as f:
        save = pickle.load(f)
        train_dataset = save['train_dataset']
        train_labels = save['train_labels']
        test_dataset = save['test_dataset']
        test_labels = save['test_labels']
        del save
        print('Training set', train_dataset.shape, train_labels.shape)
        print('Test set', test_dataset.shape, test_labels.shape)

    return [train_dataset,test_dataset], [train_labels, test_labels]


def save_model(model):
    with open('save_file.txt', 'wb') as file:
        pickle.dump(model, file)


def load_model():
    with open('save_file.txt', 'rb') as file:
        model = pickle.load(file)
        return model


def train(train_imgs, train_labels):
    '''
    train_imgs   : training set of images (logos) .. shape (500,64,32,3)
    train_labels : labels of the training set .. classes [1,2,...,27]  .. shape (500, 1)
    '''
    # pca model


    # SVM model
    model = svm.SVC(kernel='linear', gamma=0.001, C=100)
    model.fit(train_imgs, train_labels)
    predicted = model.predict(train_imgs)

    accuracy = accuracy_score(train_labels, predicted) * 100.0
    print("training accuracy of the model is {} %".format(accuracy))

    save_model(model)

    return model


def predict(test_imgs, test_labels):
    model = load_model()

    predicted = model.predict(test_imgs)

    accuracy = accuracy_score(test_labels, predicted) * 100.0
    print("testing accuracy of the model is {} %".format(accuracy))


    
def main():

    # read input data
    dataset, labels = read_data()

    train_dataset, train_labels = reformat(dataset[0], labels[0])
    test_dataset, test_labels = reformat(dataset[1], labels[1])
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

    train_dataset = train_dataset.reshape((50000,64*32*3))
    test_dataset = test_dataset.reshape((5000, 64*32*3))
    train_dataset = train_dataset[:20000,:]
    test_dataset = test_dataset[:2000,:]
    print(train_dataset.shape)
    print(test_dataset.shape)
    test_labels = np.argmax(test_labels,axis=1)
    train_labels = np.argmax(train_labels, axis=1)
    test_labels = test_labels[:2000]
    train_labels = train_labels[:20000]
    print(train_dataset.shape)
    print(test_dataset.shape)

    train(train_dataset, train_labels)
    predict(test_dataset, test_labels)

    


if __name__ == '__main__':
    main()
